import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-inicio',
    templateUrl: './inicio.component.html',
    styles: []
})
export class InicioComponent implements OnInit {

    public array = [1, 2, 3, 4];
    public seccion1 = ['../../../assets/ConfeccionCosplay/docpeste/IMG_20181112_211221.jpg',
        '../../../assets/ConfeccionCosplay/docpeste/IMG_20181113_150345.jpg',
        '../../../assets/ConfeccionCosplay/docpeste/IMG_20181113_175806.jpg']

    constructor() { }

    ngOnInit() {
    }

}
