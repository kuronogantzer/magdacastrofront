import { Component, OnInit } from "@angular/core";
import { faSkull } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: "app-filtros",
  templateUrl: "./filtros.component.html",
  styles: []
})
export class FiltrosComponent implements OnInit {

    precio_min = 0;
    precio_max = 1000000;
    rango_precios = [0, 1000000];
    formatoMoneda = (value: number) => `$ ${value}`;
    calificacion_min = 0;
    calificacion_max= 5;
    rango_calificacion = [0, 5];
    faSkull = faSkull;
    etiquetas = [{ label: 'Apple', value: 'Apple' },
        { label: 'Pear', value: 'Pear' },
        { label: 'Orange', value: 'Orange' },
        { label: 'Coconut', value: 'Coconut' }];
    etiquetas_seleccionadas = [];

    constructor() {}

    ngOnInit() {}

    cambio_rango_precio(event: number[]) {
        this.precio_min = event[0];
        this.precio_max = event[1];
    }

    cambio_precio_min(event: number) {
        this.rango_precios = [event, this.precio_max];
    }

    cambio_precio_max(event: number) {
        this.rango_precios = [this.precio_min, event];
    }

    cambio_rango_calificacion(event: number[]) {
      this.calificacion_min = event[0];
      this.calificacion_max = event[1];
  }

  cambio_calificacion_min(event: number) {
      this.rango_calificacion = [event, this.calificacion_max];
  }

  cambio_calificacion_max(event: number) {
      this.rango_calificacion = [this.calificacion_min, event];
  }
}
