import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { FigurasComponent } from './componentes/secciones/figuras/figuras.component';
import { CosplayComponent } from './componentes/secciones/cosplay/cosplay.component';
import { ArteComponent } from './componentes/secciones/arte/arte.component';
import { ArtistasComponent } from './componentes/secciones/artistas/artistas.component';

const routes: Routes = [
    { path: '', component: InicioComponent },
    { path: 'figuras-accesorios', component: FigurasComponent },
    { path: 'cosplay-confeccion', component: CosplayComponent },
    { path: 'obras-arte', component: ArteComponent },
    { path: 'otros-artistas', component: ArtistasComponent },
    { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
