import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

/* COMPONENTES */
import { MenuComponent } from './componentes/comunes/menu/menu.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { FigurasComponent } from './componentes/secciones/figuras/figuras.component';
import { CosplayComponent } from './componentes/secciones/cosplay/cosplay.component';
import { ArteComponent } from './componentes/secciones/arte/arte.component';
import { ArtistasComponent } from './componentes/secciones/artistas/artistas.component';
import { FiltrosComponent } from './componentes/comunes/filtros/filtros.component';
import { TarjetasComponent } from './componentes/comunes/tarjetas/tarjetas.component';

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    InicioComponent,
    FigurasComponent,
    CosplayComponent,
    ArteComponent,
    ArtistasComponent,
    FiltrosComponent,
    TarjetasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    //AngularFontAwesomeModule,
    FontAwesomeModule
  ],
  providers: [{ provide: NZ_I18N, useValue: es_ES }],
  bootstrap: [AppComponent]
})
export class AppModule { }
